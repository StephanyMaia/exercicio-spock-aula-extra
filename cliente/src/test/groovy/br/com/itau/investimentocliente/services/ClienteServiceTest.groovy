package br.com.itau.investimentocliente.services

import java.util.Optional
import br.com.itau.investimentocliente.repositories.ClienteRepository
import br.com.itau.investimentocliente.models.Cliente
import spock.lang.Specification

class ClienteServiceTest extends Specification{
	ClienteService clienteService
	ClienteRepository clienteRepository
	
	def setup() {
		clienteService = new ClienteService()
		clienteRepository = Mock()
		
		clienteService.clienteRepository = clienteRepository
	}
	
	def 'devesse salvar dados cliente'(){
		given: 'dados do cliente são informados'
		Cliente cliente = new Cliente()
		cliente.setNome('Stephany')
		cliente.setCpf('442.709.558-56')
		
		when: 'cliente será salvo'
		def clienteSalvo = clienteService.cadastrar(cliente)
		
		then: 'retorne cliente'
		1 * clienteRepository.save(_) >> cliente
		clienteSalvo != null
	}
	
	def 'deve buscar um cliente por CPF'(){
		given: 'dados do cliente existem na base'
		String cpf = '442.709.558-56'
		Cliente cliente = new Cliente()
		cliente.setNome('Stephany')
		cliente.setCpf(cpf)
		def clienteOptional = Optional.of(cliente)
		
		when: 'realiza busca informando cpf'
		def clienteEncontrado = clienteService.buscar(cpf)
		
		then: 'retorne o cliente procurado'
		1 * clienteRepository.findByCpf(_) >> clienteOptional
		clienteEncontrado.isPresent() == true
	}
}
