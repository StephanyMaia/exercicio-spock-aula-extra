package br.com.itau.investimentocliente.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.http.MediaType

import br.com.itau.investimentocliente.repositories.ClienteRepository
import br.com.itau.investimentocliente.services.ClienteService
import br.com.itau.investimentocliente.models.Cliente
import spock.lang.Specification
import spock.mock.DetachedMockFactory
import java.util.Optional

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest
class ClienteControllerTest extends Specification{
	@Autowired
	MockMvc mockMvc;

	@Autowired
	ClienteService clienteService;

	@Autowired
	ClienteRepository clienteRepository;

	def 'devesse buscar cliente por CPF'(){
		given: 'um cliente existe na base'
		String cpf = '442.709.558-56'
		Cliente cliente = new Cliente()
		cliente.setNome('Stephany')
		cliente.setCpf(cpf)
		Optional clienteOptional = Optional.of(cliente)

		when: 'busca é realizada'
		def resposta = mockMvc.perform(get('/442.709.558-56'))

		then: 'retorne apenas um cliente'
		1 * clienteService.buscar(_) >> clienteOptional
		clienteOptional.isPresent() == true
	}

	def 'devesse inserir novo cliente'(){
		given: 'os dados de um cliente são informados'
		Cliente cliente = new Cliente()
		cliente.setNome('Stephany')
		cliente.setCpf('442.709.558-56')

		when: 'novo cadastro é realizado'
		def resposta = mockMvc.perform(
				post('/')
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content('{"nome": "Stephany", "cpf": "442.709.558-56"}')
				)

		then: 'insira o cliente corretamente'
		1 * clienteService.cadastrar(_) >> cliente
		resposta.andExpect(status().isCreated())
				.andExpect(jsonPath('$.nome').value('Stephany'))
	}

	@TestConfiguration
	static class MockConfig{
		def factory = new DetachedMockFactory()

		@Bean
		ClienteService clienteService() {
			return factory.Mock(ClienteService)
		}

		@Bean
		ClienteRepository clienteRepository(){
			return factory.Mock(ClienteRepository)
		}
	}
}
